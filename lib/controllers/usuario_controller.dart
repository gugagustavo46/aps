import 'package:aps/apis/usuarios.dart';
import 'package:aps/cubit/app_cubit.dart';
import 'package:aps/models/usuarios.dart';
import 'package:aps/screens/main/main_screen.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:get/get.dart';

class UsuarioController extends GetxController {
  RxInt codusu = 0.obs;
  RxString nome = "".obs;

  RxBool isLoading = false.obs;

  void login({required String nome, required String senha}) async {
    isLoading.value = true;
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      try {
        Usuario usuario = await UsuariosApi.login(nome, senha);
        codusu.value = usuario.codusu;
        this.nome.value = usuario.nome;
        update();
        Future.delayed(const Duration(seconds: 2), () {
          print(usuario);

          Get.offAll(MainPage());
          isLoading.value = false;
        });
      } catch (e) {
        isLoading.value = false;
        Get.snackbar(
          'Erro',
          'Usuário ou senha inválidos',
          icon: const Icon(
            Icons.error,
            color: Colors.white,
          ),
          backgroundColor: Colors.red,
          colorText: Colors.white,
        );
      }
    } else {
      isLoading.value = false;
      Get.snackbar(
        "Sem conexão",
        "Verifique sua conexão com a internet",
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.white,
        colorText: Colors.black,
        icon: const Icon(
          Icons.network_wifi_rounded,
          color: Colors.red,
        ),
      );
      return;
    }
  }
}
