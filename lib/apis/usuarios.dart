import 'dart:convert';
import 'dart:io';

import 'package:aps/models/usuarios.dart';
import 'package:dio/dio.dart';

abstract class UsuariosApi {
  static Future<Usuario> login(String nome, String senha) async {
    var response = await Dio().putUri(
      Uri(
        scheme: "http",
        host: '192.168.15.15',
        port: 3770,
        path: "/login",
      ),
      options: Options(headers: {
        HttpHeaders.contentTypeHeader: "application/json",
      }),
      data: json.encode({
        "usuario": nome.toUpperCase().trim(),
        "senha": senha,
      }),
    );
    if (response.statusCode == 200) {
      return Usuario.fromMap(response.data as Map<String, dynamic>);
    } else {
      throw Exception('Usuário ou senha inválidos');
    }
  }
}
