import 'dart:convert';

import 'package:aps/models/data_model.dart';
import 'package:http/http.dart' as http;

class DataServices {
  String baseUrl = 'http://192.168.15.15:8481';

  //String baseUrl = 'http://mark.bslmeiyu.com/api';

  Future<List<DataModel>> getInfo() async {
    //var apiUrl = '/getplaces';
    var apiUrl = '/campus';
    http.Response res = await http.get(Uri.parse(baseUrl + apiUrl));
    try {
      if (res.statusCode == 200) {
        List<dynamic> list = jsonDecode(res.body);
        print(list);
        return list.map((e) => DataModel.fromJson(e)).toList();
      } else {
        return <DataModel>[];
      }
    } catch (e) {
      print(e);
      return <DataModel>[];
    }
  }
}
