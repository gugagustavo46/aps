import 'package:aps/cubit/app_cubit.dart';
import 'package:aps/cubit/app_cubit_logics.dart';
import 'package:aps/screens/details/detail_page.dart';
import 'package:aps/screens/login/login_screen.dart';
import 'package:aps/screens/main/main_screen.dart';
import 'package:aps/services/data_services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:sizer/sizer.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return GetMaterialApp(
          title: 'Frota App',
          debugShowCheckedModeBanner: false,
          home: BlocProvider<AppCubits>(
            create: (context) => AppCubits(
              data: DataServices(),
            ),
            child: AppCubitLogics(),
          ));
    });
  }
}
