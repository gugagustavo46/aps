import 'package:aps/cubit/app_cubit_states.dart';
import 'package:aps/models/data_model.dart';
import 'package:aps/screens/login/login_screen.dart';
import 'package:aps/services/data_services.dart';
import 'package:bloc/bloc.dart';

class AppCubits extends Cubit<CubitStates> {
  AppCubits({required this.data}) : super(InitialState()) {
    emit(WelcomeState());
  }

  final DataServices data;

  late final places;
  void getData() async {
    try {
      emit(LoadingState());
      places = await data.getInfo();
      emit(LoadedState(places));
    } catch (e) {
      print(e);
    }
  }

  detailPage(DataModel data) {
    emit(DetailsState(data));
  }

  goHome() {
    emit(LoadedState(places));
  }
}
