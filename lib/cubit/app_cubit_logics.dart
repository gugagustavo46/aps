import 'package:aps/cubit/app_cubit.dart';
import 'package:aps/cubit/app_cubit_states.dart';
import 'package:aps/screens/details/detail_page.dart';
import 'package:aps/screens/home/home_screen.dart';
import 'package:aps/screens/login/login_screen.dart';
import 'package:aps/screens/main/main_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AppCubitLogics extends StatefulWidget {
  AppCubitLogics({Key? key}) : super(key: key);

  @override
  State<AppCubitLogics> createState() => _AppCubitLogicsState();
}

class _AppCubitLogicsState extends State<AppCubitLogics> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<AppCubits, CubitStates>(builder: (context, state) {
        if (state is WelcomeState) {
          return LoginScreen();
        }
        if (state is DetailsState) {
          return DetailPage();
        }
        if (state is LoadingState) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if (state is LoadedState) {
          return MainPage();
        } else {
          return Container();
        }
      }),
    );
  }
}
