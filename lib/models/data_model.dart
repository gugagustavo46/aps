// ignore_for_file: public_member_api_docs, sort_constructors_first
/*
class DataModel {

  String name;
  String img;
  int price;
  int people;
  int stars;
  String description;
  String location;

  DataModel({
    required this.name,
    required this.img,
    required this.price,
    required this.people,
    required this.stars,
    required this.description,
    required this.location,
  });

  factory DataModel.fromJson(Map<String, dynamic> json){
    return DataModel(name: json["name"], 
    img: json["img"], 
    price: json["price"], 
    people: json["people"], 
    stars:json["stars"], 
    description: json["description"], 
    location: json["location"]);
  }



}
*/

class DataModel {
  String nome;
  String endereco;
  String numero;
  String complemento;
  String cep;
  String bairro;
  String cidade;
  String estado;
  String regiao;
  String telefone;
  String localizacao;

  DataModel(
      {required this.nome,
      required this.endereco,
      required this.numero,
      required this.complemento,
      required this.cep,
      required this.bairro,
      required this.cidade,
      required this.estado,
      required this.regiao,
      required this.telefone,
      required this.localizacao});

  factory DataModel.fromJson(Map<String, dynamic> json) {
    return DataModel(
      nome: json["nome"],
      endereco: json["endereco"],
      numero: json["numero"],
      complemento: json["complemento"],
      cep: json["cep"],
      bairro: json["bairro"],
      cidade: json["cidade"],
      estado: json["estado"],
      regiao: json["regiao"],
      telefone: json["telefone"],
      localizacao: json["localizacao"],
    );
  }
}
