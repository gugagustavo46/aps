import 'dart:convert';

class Usuario {
  final int codusu;
  final String nome;

  Usuario({
    required this.codusu,
    required this.nome,
  });

  Usuario copyWith({
    int? codusu,
    String? nome,
  }) {
    return Usuario(
      codusu: codusu ?? this.codusu,
      nome: nome ?? this.nome,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'codusu': codusu,
      'nome': nome,
    };
  }

  factory Usuario.fromMap(Map<String, dynamic> map) {
    return Usuario(
      codusu: map['codusu']?.toInt() ?? 0,
      nome: map['nome'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory Usuario.fromJson(String source) =>
      Usuario.fromMap(json.decode(source));

  @override
  String toString() => 'Usuario(codusu: $codusu, nome: $nome)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Usuario && other.codusu == codusu && other.nome == nome;
  }

  @override
  int get hashCode => codusu.hashCode ^ nome.hashCode;
}
