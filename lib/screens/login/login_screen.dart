import 'dart:io';

import 'package:aps/controllers/usuario_controller.dart';
import 'package:aps/cubit/app_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _passwordVisibility = false;
  final _formKey = GlobalKey<FormState>();
  final usuarioController = Get.put(UsuarioController());
  final _usuarioController = TextEditingController();
  final _senhaController = TextEditingController();

  void changePasswordVisibility() {
    setState(() {
      _passwordVisibility = !_passwordVisibility;
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => exit(0),
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Center(
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(30),
              child: Center(
                child: SizedBox(
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Image.asset(
                          'assets/images/cubix_logo.png',
                          fit: BoxFit.contain,
                        ),
                        SizedBox(
                          height: 2.h,
                        ),
                        const Text(
                          'Bem-vindo',
                          style: TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 3.h),
                        TextFormField(
                          controller: _usuarioController,
                          decoration: const InputDecoration(
                            prefixIcon: Icon(Icons.person),
                            labelText: 'Usuário',
                            labelStyle: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          validator: (value) =>
                              value!.isEmpty ? 'Digite o usuário' : null,
                        ),
                        SizedBox(height: 2.h),
                        TextFormField(
                          controller: _senhaController,
                          obscureText: !_passwordVisibility,
                          decoration: InputDecoration(
                            prefixIcon: const Icon(Icons.lock_outline),
                            suffixIcon: IconButton(
                              onPressed: changePasswordVisibility,
                              icon: Icon(
                                _passwordVisibility
                                    ? Icons.visibility
                                    : Icons.visibility_off,
                              ),
                            ),
                            labelText: 'Senha',
                            labelStyle: const TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          validator: (value) => value!.isEmpty
                              ? 'Digite a senha'
                              : value.length < 6
                                  ? 'A senha deve ter no mínimo 6 caracteres'
                                  : null,
                        ),
                        SizedBox(height: 2.h),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 20,
                              vertical: 10,
                            ),
                          ),
                          child: const Text(
                            'Entrar',
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          onPressed: () {
                            //  usuarioController.login(
                            //    nome: _usuarioController.text,
                            //    senha: _senhaController.text,
                            //  );
                            BlocProvider.of<AppCubits>(context).getData();
                          },
                        ),
                        Obx(
                          (() => Column(
                                children: [
                                  SizedBox(
                                    height: 2.h,
                                  ),
                                  usuarioController.isLoading.isTrue
                                      ? const CircularProgressIndicator()
                                      : Container(),
                                ],
                              )),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
